# desafio-api-rest

Desafio API Rest
Desafio Rest Assured Sicred

Projeto Simulação de Crédito utilizado para Simular a tomada de empréstimo

Automação de testes utilizando o Rest Assured com a IDE Intellij Idea e linguagem Java, Gerenciador de dependencias Maven e Java versão 11.0. Escrita dos cenários realizada em BDD.

Cenários de Teste

Dado que realizo a consulta do CPF <br />
Quando o CPF informado não possui restrição <br />
Então o status 204 é retornado <br />

Dado que realizo a consulta do CPF <br />
Quando o CPF informado possui restrição <br />
Então o status 200 é retornado com a mensagem “O CPF 99999999999 possui restrição” <br />

Dado que realizo o cadastro de uma nova simulação <br />
Quando informo os atributos obrigatórios <br />
Então o status 201 é retornado com os dados inseridos no cadastro <br />

Dado que realizo o cadastro de uma nova simulação <br />
Quando não informo os atributos corretamente <br />
Então o status 400 é retornado com a l4ista de erros <br />

Dado que realizo o cadastro de uma nova simulação <br />
Quando informo o número de um CPF cadastrado <br />
Então o status 409 é retornado com a mensagem “CPF já existente” <br />

Dado que informo o CPF de uma simulação existente <br />
Quando realizo a alteração de qualquer dado do atributo <br />
Então a alteração é realizada com sucesso <br />

Dado que informo o CPF de uma simulação inexistente <br />
Quando realizo a alteração de qualquer dado do atributo <br />
Então o status 404 é retornado com a mensagem “CPF não encontrado” <br />

Dado que realizo uma consulta <br />
Quando existir uma ou mais simulação cadastradas <br />
Então é retornado uma listagem com as simulações cadastradas <br />

Dado que realizo uma consulta <br />
Quando não existir simulação cadastrada <br />
Então o status 204 é retornado <br />

Dado que informo o CPF de uma simulação existente <br />
Quando realizo a consulta da simulação <br />
Então é retornada a simulação cadastrada <br />

Dado que informo o CPF de uma simulação inexistente <br />
Quando realizo a consulta da simulação <br />
Então o status 404 é retornado <br />

Dado que informo o ID da simulação cadastrada <br />
Quando realizo a exclusão <br />
Então o status 204 é retornado <br />

Dado que informo o ID da simulação não cadastrada <br />
Quando realizo a exclusão <br />
Então o status 404 é retornado com a mensagem “Simulação não encontrada” <br />
